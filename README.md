# Project Name: Batch-248-Capstone-2-Suplito E-commerce API
- An e-commerce API designed to manage products, carts, orders, and users.

Regular User Credentials:
- email: "user@mail.com"
- password: "user1234"

Admin Credentials:
- email: "admin@mail.com"
- password: "admin1234"

# Features:

Authentication Middleware:
- Ensures all Admin and Users are authenticated
- Error handling is set up for all code

Authentication Routes & Controllers:
- User Registration
- Check if Email Exists
- Password Bcrypt Hashing
- CartId association
- User Authentication
- Requires all fields to be filled out
- Fields Validation
- Generates, verifies, and decodes a token

User Routes & Controllers:
- Retrieve all user profiles (Admin only)
- Retrieve user profile (Admin-all / User-self)
- Update user profile (Admin-all / User-self)
- User restriction:
- Cannot set self as an admin
- Cannot access other users’ profiles
- Admin can promote users to become admins
- Delete a user (Admin-all / User-self)
- User restriction:
- Cannot delete other users’ profiles

Product Routes & Controllers:
- Create Product (Admin only)
- Retrieve all active products (Admin & User)
- Retrieve all products (Admin & User)
- Using query params ?includeNonActive=true
- Using query params ?keyword=value
- Retrieve single product (Admin & User)
- Update Product information (Admin only)
- Can update all available fields
- Can archive & unarchive a product

Cart Routes & Controllers:
- Create add to cart (User Only)
- Total products base on quantity
- If same product added quantity will just increase
- Get all list of users cart (Admin only)
- Get specific user cart (Admin-all / User-self)
- Delete all users cart Items (Admin only)
- Update users cart items (Admin-all / User-self)
- Quantity can be increased or decreased
- Total price should adjust in accordance with the quantity
- Setting quantity to 0 removes product from the cart

Order Routes & Controllers:
- Create Order / Checkout (Users Only)
- Retrieve all products from the user’s cart
- Decrease each product's quantity by the number purchased
- Associate the OrderId and quantity of every purchase with each product
- Mark the OrderId and order status in associated user profile
- Empty the cart
- Retrieve user’s orders (Admin-all / User-self)
- Retrieve all orders (Admin-only)
- Update orders (Admin-all / User-self)
- Users can only choose to cancel orders.
- Users cannot cancel orders if the order status is not pending.
- The order status of the user's reference should be automatically updated.