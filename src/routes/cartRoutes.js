const express = require("express");
const router = express.Router();
const cartController = require("../controllers/cartController");
const {
  checkPermissionFor,
  actionTypes,
} = require("../middleware/authMiddleware");

router.get(
  "/",
  checkPermissionFor(actionTypes.GET_CARTS_ALL),
  cartController.getAllCarts
);

router.get(
  "/:id",
  checkPermissionFor(actionTypes.GET_CART_BY_ID),
  cartController.getUserCart
);

router.post(
  "/:id",
  checkPermissionFor(actionTypes.ADD_TO_CART_BY_ID),
  cartController.addToCart
);

router.patch(
  "/:id",
  checkPermissionFor(actionTypes.UPDATE_CART_BY_ID),
  cartController.updateUserCartItems
);

router.delete(
  "/:id",
  checkPermissionFor(actionTypes.DELETE_CART_BY_ID),
  cartController.deleteUserAllCartItems
);

module.exports = router;
