const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderController");
const {
  checkPermissionFor,
  actionTypes,
} = require("../middleware/authMiddleware");

router.post(
  "/",
  checkPermissionFor(actionTypes.CREATE_ORDER),
  orderController.createOrder
);

router.get(
  "/",
  checkPermissionFor(actionTypes.GET_ORDERS_ALL),
  orderController.getAllOrders
);

router.get(
  "/:id",
  checkPermissionFor(actionTypes.GET_ORDER_BY_ID),
  orderController.getOrderById
);

router.put(
  "/",
  checkPermissionFor(actionTypes.UPDATE_ORDER_BY_ID),
  orderController.updateOrdersById
);

module.exports = router;
