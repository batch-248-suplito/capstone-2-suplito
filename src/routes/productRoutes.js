const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const {
  checkAuth,
  checkPermissionFor,
  actionTypes,
} = require("../middleware/authMiddleware");
const { uploadProductImages } = require("../config/multerConfig");

router.post(
  "/",
  checkAuth,
  checkPermissionFor(actionTypes.CREATE_PRODUCTS),
  productController.createProduct
);

router.post(
  "/:productId/images",
  checkAuth,
  checkPermissionFor(actionTypes.POST_PRODUCT_IMAGE),
  uploadProductImages.array("images"),
  productController.postProductImage
);

router.get("/", productController.getAllProducts);

router.get("/:id", productController.getSingleProduct);

router.patch(
  "/:id",
  checkAuth,
  checkPermissionFor(actionTypes.UPDATE_PRODUCT_BY_ID),
  productController.updateProduct
);

router.delete(
  "/:id",
  checkAuth,
  checkPermissionFor(actionTypes.DELETE_PRODUCT_BY_ID),
  productController.deleteProduct
);

router.delete(
  "/:productId/images/:imageKey",
  checkAuth,
  checkPermissionFor(actionTypes.DELETE_PRODUCT_IMAGES_BY_ID),
  productController.deleteProductImages
);

module.exports = router;
