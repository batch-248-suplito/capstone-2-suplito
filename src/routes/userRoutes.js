const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const {
  checkPermissionFor,
  actionTypes,
} = require("../middleware/authMiddleware");
const { uploadUsersImages } = require("../config/multerConfig");

router.get(
  "/",
  checkPermissionFor(actionTypes.GET_USERS_ALL),
  userController.getAllUsers
);

router.get(
  "/:id",
  checkPermissionFor(actionTypes.GET_USER_BY_ID),
  userController.getUserProfile
);

router.patch(
  "/:id",
  checkPermissionFor(actionTypes.UPDATE_USER_BY_ID),
  userController.updateUser
);

router.delete(
  "/:id",
  checkPermissionFor(actionTypes.DELETE_USER_BY_ID),
  userController.deleteUser
);

router.post(
  "/:userId/images",
  checkPermissionFor(actionTypes.POST_USER_IMAGE),
  uploadUsersImages.array("images"),
  userController.postUserImage
);

module.exports = router;
