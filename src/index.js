// Import dependencies
const express = require("express");
const cors = require("cors");
const cookieParser = require("cookie-parser");
const { checkAuth } = require("./middleware/authMiddleware");

// Connection to MongoDB Atlas
const db = require("./config/database");

// Create Express app
const app = express();

const corsOptions = {
  origin: process.env.ORIGIN.split(","),
  credentials: true,
};

// Middleware
app.use(cors(corsOptions)); // enable CORS
app.use(express.json()); // parse JSON requests
app.use(cookieParser()); // to use token from cookie

// Import routes
const authRoutes = require("./routes/authRoutes");
app.use("/api/auth", authRoutes);

const userRoutes = require("./routes/userRoutes");
app.use("/api/users", checkAuth, userRoutes);

const productRoutes = require("./routes/productRoutes");
app.use("/api/products", productRoutes);

const cartRoutes = require("./routes/cartRoutes");
app.use("/api/cart", checkAuth, cartRoutes);

const orderRoutes = require("./routes/orderRoutes");
app.use("/api/orders", checkAuth, orderRoutes);

// Server
const port = process.env.PORT || 4000;
app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});
