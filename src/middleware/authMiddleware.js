// dependencies
const jwt = require("../config/jwt");

const actionTypes = {
  // users endpoints
  GET_USERS_ALL: "get.users.all",
  GET_USER_BY_ID: "get.user.by.id",
  UPDATE_USER_BY_ID: "update.user.by.id",
  POST_USER_IMAGE: "post.user.image",
  DELETE_USER_BY_ID: "delete.user.by.id",
  // products endpoints
  CREATE_PRODUCTS: "create.product",
  GET_PRODUCTS_ALL: "get.products.all",
  GET_PRODUCT_BY_ID: "get.product.by.id",
  UPDATE_PRODUCT_BY_ID: "update.product.by.id",
  POST_PRODUCT_IMAGE: "post.product.image",
  DELETE_PRODUCT_BY_ID: "delete.product.by.id",
  DELETE_PRODUCT_IMAGES_BY_ID: "delete.product.images.by.id",
  // carts endpoints
  GET_CARTS_ALL: "get.carts.all",
  GET_CART_BY_ID: "get.cart.by.id",
  ADD_TO_CART_BY_ID: "add.to.cart.by.id",
  UPDATE_CART_BY_ID: "update.cart.by.id",
  DELETE_CART_BY_ID: "delete.cart.by.id",
  // orders endpoints
  CREATE_ORDER: "create.order",
  GET_ORDERS_ALL: "get.orders.all",
  GET_ORDER_BY_ID: "get.order.by.id",
  UPDATE_ORDER_BY_ID: "update.order.by.id",
};

module.exports.actionTypes = actionTypes;

const denyAccess = (res, details) => {
  res.status(403).json({ message: "Access Denied.", details });
};

const denySelfAccess = (req, res, next, details) => {
  const { userId, cartId } = req.user;
  const { id } = req.params;
  const isOwnUserId = id === userId;
  const isOwncartId = id === cartId;
  if (isOwnUserId || isOwncartId) {
    denyAccess(res, details);
  } else {
    next();
  }
};

const allowSelfAccess = (req, res, next, details) => {
  const { userId, cartId } = req.user;
  const { id } = req.params;
  const isOwnUserId = id === userId;
  const isOwncartId = id === cartId;
  if (isOwnUserId || isOwncartId || userId) {
    next();
  } else {
    denyAccess(res, details);
  }
};

// verify & decode token
module.exports.checkAuth = (req, res, next) => {
  const token = req.cookies.access_token;
  if (!token) {
    return res.status(401).json({
      message: "Authentication failed. Please login or sign up to continue.",
    });
  }

  try {
    const decodedToken = jwt.verifyToken(token);
    req.user = decodedToken;
    next();
  } catch (error) {
    return res
      .status(401)
      .json({ message: "Authentication failed. Invalid or expired token." });
  }
};

// permissions
module.exports.checkPermissionFor = (action) => (req, res, next) => {
  // Admin role permissions
  if (req.user.isAdmin) {
    if (action === actionTypes.GET_USERS_ALL) {
      next();
    }
    if (action === actionTypes.GET_USER_BY_ID) {
      next();
    }
    if (action === actionTypes.UPDATE_USER_BY_ID) {
      next();
    }
    if (action === actionTypes.DELETE_USER_BY_ID) {
      next();
    }
    if (action === actionTypes.POST_USER_IMAGE) {
      next();
    }
    // products
    if (action === actionTypes.CREATE_PRODUCTS) {
      next();
    }
    if (action === actionTypes.GET_PRODUCTS_ALL) {
      next();
    }
    if (action === actionTypes.UPDATE_PRODUCT_BY_ID) {
      next();
    }
    if (action === actionTypes.DELETE_PRODUCT_BY_ID) {
      next();
    }
    if (action === actionTypes.GET_PRODUCT_BY_ID) {
      next();
    }
    if (action === actionTypes.DELETE_PRODUCT_IMAGES_BY_ID) {
      next();
    }
    if (action === actionTypes.POST_PRODUCT_IMAGE) {
      next();
    }
    // carts
    if (action === actionTypes.GET_CARTS_ALL) {
      next();
    }
    if (action === actionTypes.GET_CART_BY_ID) {
      next();
    }
    if (action === actionTypes.ADD_TO_CART_BY_ID) {
      denySelfAccess(
        req,
        res,
        next,
        "Admin is not authorized to add or update their own cart."
      );
    }
    if (action === actionTypes.UPDATE_CART_BY_ID) {
      denySelfAccess(
        req,
        res,
        next,
        "Admin is not authorized to add or update their own cart."
      );
    }
    if (action === actionTypes.DELETE_CART_BY_ID) {
      denySelfAccess(
        req,
        res,
        next,
        "Admin is not authorized to delete their own cart."
      );
    }
    // orders
    if (action === actionTypes.CREATE_ORDER) {
      denyAccess(res, "Admin is not authorized to create their own order.");
    }
    if (action === actionTypes.GET_ORDER_BY_ID) {
      denySelfAccess(req, res, next, "Admin do not have order features.");
    }
    if (action === actionTypes.GET_ORDERS_ALL) {
      next();
    }
    if (action === actionTypes.UPDATE_ORDER_BY_ID) {
      denySelfAccess(
        req,
        res,
        next,
        "Admin do not have order features to update."
      );
    }
  }

  // User role permissions
  if (!req.user.isAdmin) {
    if (action === actionTypes.GET_USERS_ALL) {
      denyAccess(res);
    }
    if (action === actionTypes.GET_USER_BY_ID) {
      allowSelfAccess(
        req,
        res,
        next,
        "You are not authorized to view other user's private profile."
      );
    }
    if (action === actionTypes.UPDATE_USER_BY_ID) {
      if (req.body.hasOwnProperty("isAdmin")) {
        denyAccess(res, "You are not authorized to grant admin rights.");
      }
      allowSelfAccess(
        req,
        res,
        next,
        "You are not authorized to update another user's profile."
      );
    }
    if (action === actionTypes.DELETE_USER_BY_ID) {
      allowSelfAccess(
        req,
        res,
        next,
        "You are not authorized to delete another user's profile."
      );
    }
    if (action === actionTypes.POST_USER_IMAGE) {
      allowSelfAccess(
        req,
        res,
        next,
        "You are not authorized to add image to other user's profile."
      );
    }
    // products
    if (action === actionTypes.CREATE_PRODUCTS) {
      denyAccess(res);
    }
    if (action === actionTypes.GET_PRODUCTS_ALL) {
      next();
    }
    if (action === actionTypes.UPDATE_PRODUCT_BY_ID) {
      denyAccess(res);
    }
    if (action === actionTypes.DELETE_PRODUCT_BY_ID) {
      denyAccess(res);
    }
    if (action === actionTypes.GET_PRODUCT_BY_ID) {
      next();
    }
    if (action === actionTypes.DELETE_PRODUCT_IMAGES_BY_ID) {
      denyAccess(res);
    }
    if (action === actionTypes.POST_PRODUCT_IMAGE) {
      denyAccess(res);
    }
    // carts
    if (action === actionTypes.GET_CARTS_ALL) {
      allowSelfAccess(req, res, next, "You cannot view other users cart.");
    }
    if (action === actionTypes.GET_CART_BY_ID) {
      allowSelfAccess(req, res, next, "You cannot view other users cart.");
    }
    if (action === actionTypes.ADD_TO_CART_BY_ID) {
      allowSelfAccess(
        req,
        res,
        next,
        "You cannot add items in other users cart."
      );
    }
    if (action === actionTypes.UPDATE_CART_BY_ID) {
      allowSelfAccess(
        req,
        res,
        next,
        "You are not authorized to update another user's cart items."
      );
    }
    if (action === actionTypes.DELETE_CART_BY_ID) {
      allowSelfAccess(
        req,
        res,
        next,
        "You are not authorized to delete another user's orders."
      );
    }
    // orders
    if (action === actionTypes.CREATE_ORDER) {
      next();
    }
    if (action === actionTypes.GET_ORDER_BY_ID) {
      allowSelfAccess(
        req,
        res,
        next,
        "You are not authorized to view another user's orders."
      );
    }
    if (action === actionTypes.GET_ORDERS_ALL) {
      denyAccess(res, "You are not authorized to view another user's orders.");
    }
    if (action === actionTypes.UPDATE_ORDER_BY_ID) {
      allowSelfAccess(
        req,
        res,
        next,
        "You are not authorized to update another user's orders."
      );
    }
  }
};
