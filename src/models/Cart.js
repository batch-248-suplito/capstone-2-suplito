const mongoose = require("mongoose");
const cartSchema = new mongoose.Schema({
  userName: {
    type: String,
  },
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: [true, "User Id is required"],
  },
  cartItems: [
    {
      productId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Product",
      },
      manufacturerAddress: {
        type: String,
        required: true,
      },
      imageUrl: {
        type: String,
      },
      productName: {
        type: String,
      },
      productBrand: {
        type: String,
      },
      productPrice: {
        type: Number,
        default: 0,
      },
      quantity: {
        type: Number,
      },
      totalPrice: {
        type: Number,
        default: 0,
      },
      addedOn: {
        type: Date,
        default: new Date(),
      },
    },
  ],
});

const Cart = mongoose.model("Cart", cartSchema);
module.exports = Cart;
