const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  firstName: {
    type: String,
    required: [true, "First name is required"],
  },
  lastName: {
    type: String,
    required: [true, "Last name is required"],
  },
  email: {
    type: String,
    required: [true, "Email is required"],
  },
  password: {
    type: String,
    required: [true, "Password is required"],
  },
  mobileNo: {
    type: String,
    required: [true, "Mobile No is required"],
  },
  address: {
    type: String,
    required: [true, "address is required"],
  },
  isAdmin: {
    type: Boolean,
    default: false,
  },
  cartId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Cart",
  },
  imageDetails: {
    bucket: {
      type: String,
    },
    contentType: {
      type: String,
    },
    etag: {
      type: String,
    },
    key: {
      type: String,
    },
    location: {
      type: String,
    },
    size: {
      type: Number,
    },
  },
  orders: [
    {
      orderId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Order",
      },
    },
  ],
});

const User = mongoose.model("User", userSchema);
module.exports = User;
