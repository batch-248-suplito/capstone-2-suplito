const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
  userName: {
    type: String,
    required: true,
  },
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true,
  },
  userCartId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Cart",
    required: true,
  },
  userAddress: {
    type: String,
    required: true,
  },
  purchasedProducts: [
    {
      productId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Product",
      },
      manufacturerAddress: {
        type: String,
        required: true,
      },
      productBrand: {
        type: String,
      },
      imageUrl: {
        type: String,
      },
      productName: {
        type: String,
      },
      quantity: {
        type: Number,
      },
      totalPrice: {
        type: Number,
        default: 0,
      },
    },
  ],
  totalPurchasedAmount: {
    type: Number,
    default: 0,
    required: true,
  },
  purchasedOn: {
    type: Date,
    default: new Date(),
    required: true,
  },
  status: {
    type: String,
    enum: ["PENDING", "PROCESSED", "SHIPPED", "DELIVERED", "CANCELLED"],
    default: "PENDING",
  },
});

const Order = mongoose.model("Order", orderSchema);
module.exports = Order;
