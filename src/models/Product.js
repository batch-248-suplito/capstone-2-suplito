const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, "Name is required"],
  },
  brand: {
    type: String,
    required: [true, "Product brand is required."],
  },
  description: {
    type: String,
    required: [true, "Description is required"],
  },
  price: {
    type: Number,
    required: [true, "Price is required"],
  },
  isActive: {
    type: Boolean,
    default: true,
  },
  manufacturerAddress: {
    type: String,
    required: [true, "Manufacturer address is required."],
  },
  stocks: {
    type: Number,
    default: 0,
  },
  createdOn: {
    type: Date,
    default: new Date(),
  },
  imageDetails: [
    {
      bucket: {
        type: String,
      },
      contentType: {
        type: String,
      },
      etag: {
        type: String,
      },
      key: {
        type: String,
      },
      location: {
        type: String,
      },
      size: {
        type: Number,
      },
    },
  ],
  orders: [
    {
      orderId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Order",
        required: true,
      },
      quantity: {
        type: Number,
        required: true,
      },
    },
  ],
});

const Product = mongoose.model("Product", productSchema);
module.exports = Product;
