// Import dependencies
const mongoose = require('mongoose');
const dotenv = require('dotenv');

// Load .env
dotenv.config();

// Mongoose options
mongoose.set('strictQuery',true);

const db = mongoose.connect(process.env.MONGODB_URI, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})
.then(() => console.log("Connected to the MongoDB Atlas Database"))
.catch((err) => {
	console.log(err);
});

// Export the Mongoose instance
module.exports = db;