// Import dependencies
const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');

// Load .env
dotenv.config();

// jwt
module.exports.generateToken = (user) => {
    const { _id, email, isAdmin, cartId } = user;
    const payload = {
        userId: _id,
        email,
        isAdmin,
        cartId
    };

    return jwt.sign(payload, process.env.JWT_SECRET);
};

module.exports.verifyToken = (token) => {
    return jwt.verify(token, process.env.JWT_SECRET);
};
