const Cart = require("../models/Cart");
const User = require("../models/User");
const Product = require("../models/Product");

// add new items to cart && user cannot add to other user cart && via cartID
module.exports.addToCart = async (req, res) => {
  try {
    const cartId = req.params.id;
    const existingUser = await User.findOne({ cartId: cartId });
    if (!existingUser) {
      return res.status(404).json({ message: "User not found" });
    }

    const userName =
      existingUser.id == req.user.userId
        ? "your"
        : `user ${existingUser.firstName} ${existingUser.lastName}'s`;

    const { productId, quantity } = req.body;
    if (!productId) {
      return res.status(400).json({ message: "productId not provided." });
    }
    if (!quantity) {
      return res.status(400).json({ message: "Quantity not provided." });
    }

    const productDetails = await Product.findById(productId);
    if (productDetails.stocks === 0 || productDetails.isActive === false) {
      return res
        .status(400)
        .json({ message: "This product is not available for purchase." });
    }

    const totalPrice = productDetails.price * quantity;

    const cart = await Cart.findById(cartId);

    let cartItemIndex = cart.cartItems.findIndex(
      (item) => item.productId == productId
    );

    if (cartItemIndex > -1) {
      // If the product already exists in the cart, we just increase its quantity
      let cartItem = cart.cartItems[cartItemIndex];
      cartItem.quantity = cartItem.quantity + quantity;
      cartItem.totalPrice = productDetails.price * cartItem.quantity;
      cart.cartItems[cartItemIndex] = cartItem;
    } else {
      // If the product doesn't exist in the cart, we add it
      cart.cartItems.push({
        productId,
        manufacturerAddress: productDetails.manufacturerAddress,
        productName: productDetails.name,
        productBrand: productDetails.brand,
        productPrice: productDetails.price,
        imageUrl: productDetails.imageDetails[0].location,
        quantity,
        totalPrice,
      });
    }

    cart.cartItems.sort((a, b) => b.addedOn - a.addedOn);

    const updatedCart = await cart.save();
    res.status(200).json({
      message: `Successfully added ${quantity} ${productDetails.name} to ${userName} cart.`,
      cartItems: updatedCart.cartItems,
    });
  } catch (error) {
    console.error(error);
    res
      .status(500)
      .json({ message: "Failed to add the product to the cart. " });
  }
};

// get all users carts
module.exports.getAllCarts = async (req, res) => {
  try {
    const listOfAllUsersCarts = await Cart.find({}).lean();

    const cartsWithAdminStatus = await Promise.all(
      listOfAllUsersCarts.map(async (cart) => {
        const user = await User.findById(cart.userId);
        cart.isAdmin = user.isAdmin;
        return cart;
      })
    );

    const sortedCarts = cartsWithAdminStatus.sort((a, b) => {
      if (a.isAdmin && !b.isAdmin) return -1;
      if (!a.isAdmin && b.isAdmin) return 1;
      return 0;
    });

    res.status(200).json({ listOfAllUsersCarts: sortedCarts });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Error fetching users carts", error });
  }
};

// get user cart by cartId
module.exports.getUserCart = async (req, res) => {
  try {
    const userCartId = req.params.id;
    const userCart = await Cart.findById(userCartId);

    res.status(200).json({
      userCartDetails: userCart,
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Error fetching user cart" });
  }
};

// delete a users all cartItems by cartID for Admin only
module.exports.deleteUserAllCartItems = async (req, res) => {
  try {
    const cartId = req.params.id;
    const cart = await Cart.findById(cartId);
    if (!cart) {
      return res.status(404).json({ message: "Cart not found" });
    }

    if (cart.cartItems.length === 0) {
      return res.status(400).json({ message: "Cart is already empty" });
    }

    cart.cartItems = [];
    await cart.save();

    return res.status(200).json({
      message: `All items in cart has been removed successfully.`,
      deleteAllCartItems: cart.cartItems,
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Error deleting user cart items" });
  }
};

// update users cart items by cartId
module.exports.updateUserCartItems = async (req, res) => {
  try {
    const { quantity, cartItemId } = req.body;

    const cart = await Cart.findById(req.params.id);

    if (cart.cartItems.length === 0) {
      return res.status(400).json({ message: "Cart is already empty" });
    }

    const cartItem = cart.cartItems.find(
      (item) => item._id.toString() === cartItemId
    );

    if (cartItemId && !quantity) {
      cart.cartItems = cart.cartItems.filter((item) => {
        return item._id.toString() !== cartItemId;
      });

      const updatedCart = await cart.save();

      return res.status(200).json({
        message: `${cartItem.productName} has been removed`,
        updatedCartItems: updatedCart.cartItems,
      });
    }

    const updatedProductQuantity = parseInt(quantity);
    cartItem.quantity = updatedProductQuantity;
    cartItem.totalPrice = updatedProductQuantity * cartItem.productPrice;

    await cart.save();

    return res.status(200).json({
      message: `The quantity and total price of ${cartItem.productName} have been adjusted.`,
      updatedCartItems: cart.cartItems,
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Error Updating user cart items" });
  }
};
