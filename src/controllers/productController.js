const { s3 } = require("../config/multerConfig");
const Product = require("../models/Product");

//create new product & require all fields
module.exports.createProduct = async (req, res) => {
  try {
    const { name, brand, description, price, manufacturerAddress, stocks } =
      req.body;

    if (!name) {
      return res.status(400).json({ message: "Name is required." });
    }
    if (!brand) {
      return res.status(400).json({ message: "Brand is required." });
    }
    if (!description) {
      return res.status(400).json({ message: "Description is required." });
    }
    if (!price) {
      return res.status(400).json({ message: "Price is required." });
    }
    if (!manufacturerAddress) {
      return res
        .status(400)
        .json({ message: "Manufacturer Address is required." });
    }
    if (!stocks) {
      return res.status(400).json({ message: "stocks is required." });
    }

    const newProduct = new Product({
      name,
      brand,
      description,
      price,
      manufacturerAddress,
      stocks,
    });

    const savedProduct = await newProduct.save();
    res.status(201).json({
      message: `Successfully created product '${savedProduct.name}'`,
      createdProduct: savedProduct,
    });
  } catch (error) {
    console.error(error);
    res
      .status(500)
      .json({ message: "Something went wrong while creating the product" });
  }
};

// get all products and filter with keyword also if isActive: true or false
module.exports.getAllProducts = async (req, res) => {
  const { activeAndInactive, onlyInActive, keyword } = req.query;

  try {
    let defaultFilter = { isActive: true };

    if (activeAndInactive === "true") {
      defaultFilter = {};
    }

    if (onlyInActive === "true") {
      defaultFilter = { isActive: false };
    }

    let query = Product.find(defaultFilter);
    if (keyword) {
      const keywordRegex = new RegExp(`${keyword}`, "i");
      query = query.or([
        { name: keywordRegex },
        { brand: keywordRegex },
        { description: keywordRegex },
      ]);
    }
    const products = await query.exec();
    if (products.length === 0 && keyword) {
      return res.status(404).json({
        message:
          "No products found matching that keyword. Please try another search.",
      });
    }

    res.status(200).json({
      message: `${
        onlyInActive
          ? "List of Inactive products only"
          : activeAndInactive
          ? "List of All products"
          : "List of Active products only"
      } `,
      products,
    });
  } catch (error) {
    console.log("Error while finding products: ", error);
    res.status(500).json({ message: "Something went wrong" });
  }
};

// get single product by productId
module.exports.getSingleProduct = async (req, res) => {
  try {
    const singleProduct = await Product.findById(req.params.id);
    res.status(200).json({ singleProduct });
  } catch (error) {
    res.status(500).json({ message: "Something went wrong" });
  }
};

// add images
module.exports.postProductImage = async (req, res) => {
  try {
    const { productId } = req.params;
    const product = await Product.findById(productId);

    const imageObjects = req.files.map((file) => {
      return {
        bucket: file.bucket,
        contentType: file.contentType,
        etag: file.etag,
        key: file.key,
        location: file.location,
        mimeType: file.mimeType,
        originalName: file.originalName,
        size: file.size,
      };
    });

    const updatedProductWithImage = await Product.findByIdAndUpdate(
      productId,
      {
        $push: { imageDetails: { $each: imageObjects } },
      },
      { new: true }
    );

    res.status(200).json({
      message: `Successfully Added an image to product ${product.name}.`,
      updatedProductWithImage,
    });
  } catch (error) {
    res.status(500).json({ message: "Something went wrong" });
  }
};

// update a product details by orderIdd & can set active to nonactive
module.exports.updateProduct = async (req, res) => {
  try {
    const productId = req.params.id;
    const product = await Product.findById(productId);

    if (req.body.isActive && product.stocks <= 0) {
      return res.status(400).json({
        message: "Stocks need to be updated first to unarchive the product.",
      });
    }

    const updatedProduct = await Product.findByIdAndUpdate(
      productId,
      {
        $set: {
          name: req.body.name,
          brand: req.body.brand,
          description: req.body.description,
          price: req.body.price,
          isActive: req.body.stocks <= 0 ? false : req.body.isActive,
          manufacturerAddress: req.body.manufacturerAddress,
          stocks: req.body.stocks,
        },
      },
      { new: true }
    );

    if (updatedProduct) {
      const message =
        !updatedProduct.isActive && !updatedProduct.stocks
          ? `Product '${updatedProduct.name}' successfully updated and archived due to stocks being 0.`
          : !updatedProduct.isActive && updatedProduct.stocks !== 0
          ? `Product '${updatedProduct.name}' successfully updated. You can now unarchive this product.`
          : `Product '${updatedProduct.name}' successfully updated.`;

      return res.status(201).json({
        message: message,
        updatedProduct,
      });
    } else {
      return res
        .status(400)
        .json({ message: `Product with id ${productId} not found` });
    }
  } catch (error) {
    console.log("Error while updating product:", error);
    return res.status(500).json({ message: "Something went wrong" });
  }
};

// delete product by id
module.exports.deleteProduct = async (req, res) => {
  const productId = req.params.id;
  try {
    const deletedProduct = await Product.findByIdAndDelete(productId);
    if (deletedProduct) {
      res.status(200).json({
        message: `Product ${deletedProduct.name} has successfully deleted.`,
        deletedProduct,
      });
    } else {
      res
        .status(404)
        .json({ message: `Product with id ${productId} not found` });
    }
  } catch (error) {
    console.log("Error while deleting product:", error);
    res.status(500).json({ message: "Something went wrong" });
  }
};

// delete product images by id
module.exports.deleteProductImages = async (req, res) => {
  try {
    const { productId, imageKey } = req.params;
    const product = await Product.findById(productId);

    if (!product) {
      return res.status(404).json({ message: "Product not found" });
    }

    const imageIndex = product.imageDetails.findIndex(
      (imageObject) => imageObject.key === imageKey
    );

    if (imageIndex === -1) {
      return res.status(404).json({ message: "Image not found" });
    }

    // Delete the image from S3
    const params = {
      Bucket: "dev-shopohs-public/products",
      Key: imageKey,
    };

    s3.deleteObject(params, async (err, data) => {
      if (err) {
        console.log("Error deleting image from S3:", err);
        return res
          .status(500)
          .json({ message: "Error deleting image from S3" });
      }

      // Remove the image URL from the imageUrls array
      product.imageDetails.splice(imageIndex, 1);

      await product.save();
    });

    return res.status(200).json({
      message: `Image successfully deleted from product ${product.name}`,
      updatedProduct: product,
    });
  } catch (error) {
    console.log("Error while deleting product:", error);
    res.status(500).json({ message: "Something went wrong" });
  }
};
