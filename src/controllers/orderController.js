const Order = require("../models/Order");
const User = require("../models/User");
const Product = require("../models/Product");
const Cart = require("../models/Cart");

// checkout orders
module.exports.createOrder = async (req, res) => {
  try {
    const { cartId, selectedItemsArray } = req.body;
    const userCartId = await Cart.findById(cartId);
    const user = await User.findById(userCartId.userId);

    if (selectedItemsArray.length === 0) {
      return res.status(400).json({
        message: "Cannot checkout an empty cart or select items first.",
      });
    }

    // Create new order with details gathered from user's information and cart items
    const newOrder = new Order({
      userName: `${user.firstName} ${user.lastName}`,
      userId: user._id,
      userCartId: user.cartId,
      userAddress: user.address,
      purchasedProducts: selectedItemsArray.map((item) => ({
        productId: item.productId,
        manufacturerAddress: item.manufacturerAddress,
        productBrand: item.productBrand,
        productName: item.productName,
        imageUrl: item.imageUrl,
        quantity: item.quantity,
        totalPrice: item.totalPrice,
      })),
      totalPurchasedAmount: selectedItemsArray.reduce(
        (a, b) => a + b.totalPrice,
        0
      ),
    });

    // Update stocks and orders of each purchased product through its own productId
    for (const product of newOrder.purchasedProducts) {
      const { productId, quantity } = product;
      const updatedProduct = await Product.findByIdAndUpdate(
        productId,
        {
          $inc: { stocks: -quantity },
          $push: { orders: { orderId: newOrder._id, quantity: quantity } },
        },
        { new: true }
      );

      if (updatedProduct.stocks === 0) {
        await Product.findByIdAndUpdate(
          productId,
          { isActive: false },
          { new: true }
        );
      } else if (updatedProduct.stocks < 0) {
        return res.status(400).json({ message: "This product was sold out." });
      }
    }

    await newOrder.save();
    await User.findByIdAndUpdate(
      user._id,
      {
        $push: {
          orders: [
            {
              orderId: newOrder._id,
            },
          ],
        },
      },
      { new: true }
    );

    // Remove the items from the user's cart
    const updatedCartItems = userCartId.cartItems.filter((item) => {
      return !selectedItemsArray.some(
        (selectedItem) => selectedItem._id.toString() === item._id.toString()
      );
    });

    const remainingItems = await Cart.findByIdAndUpdate(
      userCartId._id,
      {
        cartItems: updatedCartItems,
      },
      { new: true }
    );

    res.status(201).json({
      message: "Order placed successfully",
      orders: newOrder,
      remainingItems,
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Internal server error" });
  }
};

// get all orders
module.exports.getAllOrders = async (req, res) => {
  try {
    const allUsersOrders = await Order.aggregate([
      {
        $lookup: {
          from: "users",
          localField: "userId",
          foreignField: "_id",
          as: "user",
        },
      },
      {
        $unwind: "$user",
      },
      {
        $group: {
          _id: "$userId",
          firstName: { $first: "$user.firstName" },
          lastName: { $first: "$user.lastName" },
          orders: {
            $push: {
              orderId: "$_id",
              status: "$status",
              purchasedProducts: "$purchasedProducts",
              totalPurchasedAmount: "$totalPurchasedAmount",
            },
          },
        },
      },
      {
        $addFields: {
          userName: {
            $concat: ["$firstName", " ", "$lastName"],
          },
        },
      },
      {
        $project: {
          userId: "$_id",
          userName: 1,
          orders: 1,
          _id: 0,
        },
      },
    ]);

    if (!allUsersOrders || allUsersOrders.length === 0) {
      return res.status(404).json({ error: "No list of orders exists yet" });
    }

    res.status(200).json({
      message: "List of all users orders",
      allUsersOrders,
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Internal server error" });
  }
};

// get order by id
module.exports.getOrderById = async (req, res) => {
  try {
    const userId = req.params.id;
    const user = await User.findById(userId);

    const isUser =
      req.user.userId === userId
        ? "your"
        : `${user.firstName} ${user.lastName}'s`;

    // Get order details for each orderId
    const userOrders = [];

    for (const order of user.orders) {
      const orderId = order.orderId.toString();

      const orderDetails = await Order.findById(orderId);

      userOrders.push(orderDetails); // Push new details into the array
    }

    const sortedOrders = userOrders.sort(
      (a, b) => b.purchasedOn - a.purchasedOn
    );

    res.status(201).json({
      message: `List of all ${isUser} orders.`,
      orderLists: sortedOrders,
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Internal server error" });
  }
};

// update order by userId
module.exports.updateOrdersById = async (req, res) => {
  try {
    const { orderId, newStatus } = req.body;
    if (!orderId || !newStatus) {
      return res.status(404).json({ message: `orderId and status required.` });
    }

    const currentOrder = await Order.findById(orderId);
    if (!currentOrder) {
      return res.status(404).json({
        message: `Could not find this order.`,
      });
    }

    if (currentOrder.status === newStatus) {
      return res.status(400).json({
        message: `This order's status is already ${newStatus}.`,
      });
    }

    // Update the status of the order with the new value provided.
    await Order.findByIdAndUpdate(orderId, { status: newStatus });
    const updatedOrderStatus = await Order.findById(orderId);
    res.status(200).json({
      message: `Successfully updated order status.`,
      updatedOrder: updatedOrderStatus,
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Internal server error" });
  }
};
