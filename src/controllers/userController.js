const User = require("../models/User");
const Cart = require("../models/Cart");
const bcrypt = require("bcrypt");
const Order = require("../models/Order");
const { s3 } = require("../config/multerConfig");

// This code fetches the list of all available users which can only be accessed by an admin.
module.exports.getAllUsers = async (req, res) => {
  try {
    const { keyword } = req.query;

    let query = User.find({}).select({ password: 0 });
    if (keyword) {
      const keywordRegex = new RegExp(`${keyword}`, "i");
      query = query.or([
        { firstName: keywordRegex },
        { lastName: keywordRegex },
        { email: keywordRegex },
        { address: keywordRegex },
      ]);
    }
    const listOfUsers = await query.exec();
    if (listOfUsers.length === 0 && keyword) {
      return res.status(404).json({
        message:
          "No users found matching that keyword. Please try another search.",
      });
    }

    const sortedUsers = listOfUsers.sort((a, b) => b.isAdmin - a.isAdmin);

    res.status(200).json({
      message: "Successfully retrieved the list of users.",
      listOfUsers: sortedUsers,
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Failed to retrieve the user data." });
  }
};

// retrieves user profile for either admin or authenticated user. The admin can view own or other user's profiles while the regular user can only see their own profile.
module.exports.getUserProfile = async (req, res) => {
  try {
    const userId = req.params.id;

    const userProfile = await User.findById(userId).select({ password: 0 });
    if (!userProfile) {
      return res.status(404).json({ message: "User not found" });
    }

    const isUser =
      userId === req.user.userId
        ? "your own"
        : `user ${userProfile.firstName} ${userProfile.lastName}'s`;
    res.status(200).json({
      message: `Successfully retrieved ${isUser} profile.`,
      userProfile,
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Error fetching user data" });
  }
};

/*

This function updates the user data in the database, and also checks if the user is providing the oldPassword and newPassword fields when updating the password. If no new and old password fields are provided, then it will throw an error.

*/
module.exports.updateUser = async (req, res) => {
  try {
    const userId = req.params.id;
    const userData = req.body;

    if (userData.email) {
      const existingUser = await User.findOne({ email: userData.email });
      if (existingUser && existingUser._id.toString() !== userId) {
        return res
          .status(400)
          .json({ message: "Email already exists. Please use another one." });
      }
    }

    if (userData.password || userData.newPassword) {
      if (!userData.oldPassword || !userData.newPassword) {
        return res
          .status(400)
          .json({ message: "Please provide old and new password." });
      }

      const user = await User.findById(userId);
      if (!user) {
        return res
          .status(404)
          .json({ message: `User with ID ${userId} not found` });
      }

      const passwordMatch = await bcrypt.compare(
        userData.oldPassword,
        user.password
      );
      if (!passwordMatch) {
        return res.status(401).json({ message: "Incorrect password" });
      }

      userData.password = await bcrypt.hash(userData.newPassword, 10);
      delete userData.oldPassword;
      delete userData.newPassword;
    }

    const updatedUser = await User.findByIdAndUpdate(userId, userData, {
      new: true,
    });

    if (userData.firstName || userData.lastName) {
      const fullName = `${updatedUser.firstName} ${updatedUser.lastName}`;
      await Cart.findByIdAndUpdate(
        updatedUser.cartId,
        { userName: fullName },
        { new: true }
      );
      await Order.updateMany(
        { userId: updatedUser._id },
        { userName: fullName }
      );
    }

    if (userData.address) {
      await Order.updateMany(
        { userId: updatedUser._id },
        { userAddress: updatedUser.address }
      );
    }

    if (!updatedUser) {
      return res
        .status(404)
        .json({ message: `User with ID ${userId} not found` });
    }

    const isUser =
      userId === req.user.userId
        ? "your own"
        : `user ${updatedUser.firstName} ${updatedUser.lastName}'s`;
    return res.status(200).json({
      message: `Successfully updated ${isUser} profile.`,
      updatedUser,
    });
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: "Error updating user data" });
  }
};

// deletes a user from the database using their ID, and also deletes the cart and orders associated with that user's cartId.
module.exports.deleteUser = async (req, res) => {
  try {
    const deletedUser = await User.findByIdAndDelete(req.params.id);
    if (!deletedUser) {
      return res.status(404).json({ message: "User not found" });
    }

    await Cart.findByIdAndDelete(deletedUser.cartId);
    await Order.deleteMany({ userId: req.params.id });
    const isUser =
      req.params.id === req.user.userId
        ? "your own"
        : `user ${deletedUser.firstName} ${deletedUser.lastName}'s`;
    res.status(200).json({
      message: `Successfully deleted ${isUser} profile. along with all their associated orders and the cartId registered to it as well.`,
      deletedUser,
    });
  } catch (err) {
    console.log(err);
    res.status(500).json({ message: "Error deleting user" });
  }
};

// add image to user
module.exports.postUserImage = async (req, res) => {
  try {
    const { userId } = req.params;

    const user = await User.findById(userId);
    if (user.imageDetails?.key) {
      const params = {
        Bucket: "dev-shopohs-public/users",
        Key: user.imageDetails.key,
      };
      await s3.deleteObject(params).promise();
    }

    const file = req.files[0];
    const imageObject = {
      bucket: file.bucket,
      contentType: file.contentType,
      etag: file.etag,
      key: file.key,
      location: file.location,
      mimeType: file.mimeType,
      originalName: file.originalName,
      size: file.size,
    };

    const updatedUserWithImage = await User.findByIdAndUpdate(
      userId,
      {
        imageDetails: imageObject,
      },
      { new: true }
    );

    res.status(200).json({
      message: `Successfully added/updated the image on your profile.`,
      updatedUserWithImage,
    });
  } catch (error) {
    res
      .status(500)
      .json({ message: "Failed adding/updating image to your profile." });
  }
};
