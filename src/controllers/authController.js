// import dependencies
const bcrypt = require("bcrypt");
const User = require("../models/User");
const Cart = require("../models/Cart");
const jwt = require("../config/jwt");

/*

This code performs user sign up functionality by checking for required fields, hashing the password, creating a new cart object and saving the user and cart details to relevant models in the database.

*/
module.exports.signUp = async (req, res) => {
  try {
    const { firstName, lastName, email, password, mobileNo, address } =
      req.body;

    const requiredFields = [
      "firstName",
      "lastName",
      "email",
      "password",
      "mobileNo",
      "address",
    ];

    for (let field of requiredFields) {
      if (!req.body[field]) {
        return res.status(400).json({ message: `Please provide ${field}.` });
      }
    }

    // Check if email already exists
    const existingUser = await User.findOne({ email });
    if (existingUser) {
      return res
        .status(400)
        .json({ message: "Email already exists. Please use another one." });
    }

    const hashedPassword = await bcrypt.hash(password, 10);

    const cart = new Cart({});

    const newUser = new User({
      firstName,
      lastName,
      email,
      password: hashedPassword,
      mobileNo,
      address,
      cartId: cart._id,
    });

    const userProfile = await newUser.save();

    cart.userName = `${userProfile.firstName} ${userProfile.lastName}`;
    cart.userId = userProfile._id;
    await cart.save();

    res.status(201).json({
      message: "Congratulations, you have successfully registered!",
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({
      message: "Error occurred while processing request. Please try again.",
    });
  }
};

// It checks if the email and password are valid in the request body. Validates user's email from the database. Create a token-based authentication, set it in response header and sends a success message to the client.
module.exports.login = async (req, res) => {
  const isProduction = process.env.ENVIRONMENT?.toLowerCase() === "production";

  try {
    const { email, password } = req.body;

    if (!email) {
      return res.status(400).json({ message: "Email is required" });
    }
    if (!password) {
      return res.status(400).json({ message: "Password is required" });
    }

    const user = await User.findOne({ email: email });
    if (!user) {
      return res.status(404).json({ message: "Email does not exist." });
    }

    const isMatch = await bcrypt.compare(password, user.password);
    if (!isMatch) {
      return res.status(401).json({ message: "Invalid email or password" });
    }

    const token = jwt.generateToken(user);
    res.cookie("access_token", token, {
      httpOnly: isProduction,
      secure: isProduction,
      sameSite: isProduction ? "None" : "Lax",
    });

    return res.status(200).json({
      message: "You have successfully logged in!",
      profile: user,
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({
      message: "Server error occurred while logging in. Please try again.",
    });
  }
};

//logout
module.exports.logout = async (req, res) => {
  try {
    res.cookie("access_token", "", {
      expires: new Date(0),
      path: "/",
    });

    res.cookie("CURRENT_USER", "", {
      expires: new Date(0),
      path: "/",
    });

    res.status(200).json({ message: "Logged out successfully" });
  } catch (error) {
    console.error(error);
    res.status(500).json({
      message: "Server error occurred while logging out. Please try again.",
    });
  }
};
